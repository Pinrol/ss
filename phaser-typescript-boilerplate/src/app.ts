/// <reference path="../lib/phaser.d.ts"/>
///// <reference path="_LeaLoaded.ts"/>
/// <reference path="initDataInterface.ts"/>



declare let leanderGMApi;
declare let getQueryParamValue;
declare let queryString;
let game: Phaser.Game;

// let something: string;
let multiReel;
let BET = null;

let reelTiming0 = 4000;
let reelTiming1 = 750 + 4000;
let reelTiming2 = 5000;
let yMultiplicator = -300;
let reelScrollDist = 10;
let reelStartDelay = 300;
let reelScrollDistTraveled;


let reelTween0: Phaser.Tween;
let reelTween1: Phaser.Tween;
let reelTween2: Phaser.Tween;
let reelTween3: Phaser.Tween;
let reelTween4: Phaser.Tween;

let spriteReel0: Phaser.Group;
let spriteReel1: Phaser.Group;
let spriteReel2: Phaser.Group;
let spriteReel3: Phaser.Group;
let spriteReel4: Phaser.Group;
let homeTween0;
let homeTween1;
let homeTween2;
let homeTween3;
let homeTween4;

let reelTween0_0;
let reelTween0_1;
let reelTween0_2;

let reelTween1_0;
let reelTween1_1;
let reelTween1_2;

let reelTween2_0;
let reelTween2_1;
let reelTween2_2;

let reelTween3_0;
let reelTween3_1;
let reelTween3_2;

let reelTween4_0;
let reelTween4_1;
let reelTween4_2;
let uisprite: Phaser.Sprite;

let exposionBlueGroup: Phaser.Group;
let exposionLightRedGroup: Phaser.Group;
let exposionRedGroup: Phaser.Group;
let exposionLightYellowGroup: Phaser.Group;
let exposionYellowGroup: Phaser.Group;

let reelTweenGroup: Phaser.Tween[] = new Array(5);
let groupReel: Phaser.Group[] = new Array(5);
let winRows;

let firstActivation = true;
let recievedOpenPlay = false;
let setVisibleWiningSymbols = false;

let resultArr;

let uiGroup;
let logo: Phaser.Sprite;

let mask;
let inOpenplay = false;
let inSettlePlay = false;
let spiiningReels = true;

let winRowsWinningIndexes;
let saveResponseNew;
let saveResponseOld;
let width;
let height;
let winOffset = 0;
let noWinLayout;
let subscribePlayConfirmation;
let apiHandler = { sessionID: null };
class SimpleGame {
    // game:Phaser.Game;
    initData // : IinitData;
    balanceAmount;
    winAmount;
    betAmount;
    cursors: Phaser.CursorKeys;

    spamLogs: boolean;
    myTween: Phaser.Tween;
    timer: number;

    spacebar: Phaser.Key;

    myNum = 1000;
    myArr: number[] = new Array(1);


    button;
    button2;
    tween2;
    flag = true;
    sessionId;
    constructor(initData) {
        this.initData = initData;
        console.log(this.initData + " mushimush");
        apiHandler.sessionID = leanderGMApi.getSessionId();
        BET = this.initData[0].params.betPerLineDefault * this.initData[0].params.lineLayout.length;
        this.betAmount.setTo(BET);
        game = new Phaser.Game(1920, 1080, Phaser.AUTO, "content", this);
    }

    preload() {
        console.log("i preload");
        leanderGMApi.publishEvent(leanderGMApi.publications.PRELOADING_STARTED);
        // game.load.onLoadStart.add(loadStart, this);
        /*function logProgress(progress) {
            leanderGMApi.publishEvent(
                leanderGMApi.publications.PREALOADING_PROGRESS_UPDATED,
                progress.progress * 100
            );
        }*/
        // preloadStarted();
        // leanderGMApi.publishEvent(leanderGMApi.publications.PRELOADING_STARTED);

        game.load.image("logo", "images/phaser-logo-small.png");

        game.load.image("A", "/assets/SS/PackedTextures/Images/symbols/Normal/A.png");
        game.load.image("J", "/assets/SS/PackedTextures/Images/symbols/Normal/J.png");
        game.load.image("K", "/assets/SS/PackedTextures/Images/symbols/Normal/K.png");
        game.load.image("Q", "/assets/SS/PackedTextures/Images/symbols/Normal/Q.png");
        game.load.image("10", "/assets/SS/PackedTextures/Images/symbols/Normal/10.png");
        game.load.image("Sad", "/assets/SS/PackedTextures/Images/symbols/Normal/Sad.png");
        game.load.image("Fear", "/assets/SS/PackedTextures/Images/symbols/Normal/Fear.png");
        game.load.image("Wild", "/assets/SS/PackedTextures/Images/symbols/Normal/Wild.png");
        game.load.image("Anger", "/assets/SS/PackedTextures/Images/symbols/Normal/Anger.png");
        game.load.image("Disgust", "/assets/SS/PackedTextures/Images/symbols/Normal/Disgust.png");
        leanderGMApi.publishEvent(leanderGMApi.publications.PRELOADING_PROGRESS_UPDATED, 50);
        game.load.image("Blurred_A", "/assets/SS/PackedTextures/Images/symbols/Blurred/A.png");
        game.load.image("Blurred_J", "/assets/SS/PackedTextures/Images/symbols/Blurred/J.png");
        game.load.image("Blurred_K", "/assets/SS/PackedTextures/Images/symbols/Blurred/K.png");
        game.load.image("Blurred_Q", "/assets/SS/PackedTextures/Images/symbols/Blurred/Q.png");
        game.load.image("Blurred_10", "/assets/SS/PackedTextures/Images/symbols/Blurred/10.png");
        game.load.image("Blurred_Sad", "/assets/SS/PackedTextures/Images/symbols/Blurred/Sad.png");
        game.load.image("Blurred_Fear", "/assets/SS/PackedTextures/Images/symbols/Blurred/Fear.png");
        game.load.image("Blurred_Wild", "/assets/SS/PackedTextures/Images/symbols/Blurred/Wild.png");
        game.load.image("Blurred_Anger", "/assets/SS/PackedTextures/Images/symbols/Blurred/Anger.png");
        game.load.image("Blurred_Disgust", "/assets/SS/PackedTextures/Images/symbols/Blurred/Disgust.png");

        // D:\wamp64\www\boilerPhaser2\SS\phaser-typescript-boilerplate\assets\SS\PackedTextures\Images\SumoUI
        game.load.image("frame_L", "/assets/SS/PackedTextures/Images/SumoUI/frame_L.png");
        game.load.image("frame_R", "/assets/SS/PackedTextures/Images/SumoUI/frame_R.png");
        game.load.image("frame_topR", "/assets/SS/PackedTextures/Images/SumoUI/frame_topR.png");
        game.load.image("frame_topL", "/assets/SS/PackedTextures/Images/SumoUI/frame_topL.png");
        game.load.image("frame_middle_1", "/assets/SS/PackedTextures/Images/SumoUI/frame_middle_1.png");
        game.load.image("frame_middle_2", "/assets/SS/PackedTextures/Images/SumoUI/frame_middle_2.png");
        game.load.image("frame_middle_3", "/assets/SS/PackedTextures/Images/SumoUI/frame_middle_3.png");
        game.load.image("frame_middle_4", "/assets/SS/PackedTextures/Images/SumoUI/frame_middle_4.png");

        game.load.image("logo_frame", "/assets/SS/PackedTextures/Images/SumoUI/Logo_frame.png");
        game.load.image("background", "/assets/SS/PackedTextures/Images/SumoUI/Background_Pattern.png");
        game.load.image("LightBlue_Explosion", "/assets/SS/PackedTextures/Images/SumoUI/LightBlue_WinExplosion.png");
        leanderGMApi.publishEvent(leanderGMApi.publications.PRELOADING_PROGRESS_UPDATED, 100);


        // game.load.image();
        winRows = [[1, 1, 1, 1, 1], [2, 2, 2, 2, 2], [0, 0, 0, 0, 0], [2, 1, 0, 1, 2], [0, 1, 2, 1, 0],
        [2, 2, 1, 2, 2], [0, 0, 1, 0, 0], [1, 0, 0, 0, 1], [1, 2, 2, 2, 1], [1, 2, 1, 2, 1],
        [1, 0, 1, 0, 1], [2, 1, 2, 1, 2], [0, 1, 0, 1, 0], [1, 1, 2, 1, 1], [1, 1, 0, 1, 1],
        [2, 1, 1, 1, 2], [0, 1, 1, 1, 0], [2, 1, 0, 0, 0], [0, 1, 2, 2, 2], [2, 0, 2, 0, 2],
        [0, 2, 0, 2, 0], [2, 0, 0, 0, 2], [0, 2, 2, 2, 0], [2, 2, 0, 2, 2], [0, 0, 2, 0, 0]];
        leanderGMApi.publishEvent(leanderGMApi.publications.PRELOADING_ENDED);


    }

    create() {
        // preloadStarted();

        height = 1080;
        width = 1920;
        game.stage.backgroundColor = "#4488AA";
        exposionBlueGroup = game.add.group();

        ExplosionBackgroundCreator();

        /*
        let explosion = game.add.image(game.world.centerX, game.world.centerY, "LightBlue_Explosion");
        explosion.anchor.set(0, 0.5);
        let explosion = game.add.image(game.world.centerX, game.world.centerY, "LightBlue_Explosion");
        explosion.anchor.set(0, 0.5);

        explosion.rotation = 0.22;*/

        let backGround = game.add.image(game.world.centerX, game.world.centerY + 30, "background");
        backGround.anchor.set(0.5, 0.5);
        backGround.scale.set(1.90, 1.90);


        let box = game.add.graphics(0, 0);
        box.beginFill(0x000000);
        box.drawRect(190, 125, 318, 900);
        box.alpha = 0.75;
        box.drawRect(505, 125, 300, 900);
        box.drawRect(805, 125, 300, 900);
        box.drawRect(1105, 130, 290, 895);
        box.drawRect(1395, 136, 318, 890);



        this.cursors = game.input.keyboard.createCursorKeys();

        spriteReel0 = game.add.group();
        spriteReel1 = game.add.group();
        spriteReel2 = game.add.group();
        spriteReel3 = game.add.group();
        spriteReel4 = game.add.group();

        groupReel[0] = game.add.group(spriteReel0);
        groupReel[1] = game.add.group(spriteReel1);
        groupReel[2] = game.add.group(spriteReel2);
        groupReel[3] = game.add.group(spriteReel3);
        groupReel[4] = game.add.group(spriteReel4);

        reelTweenGroup[0] = game.add.tween(reelTween0);
        reelTweenGroup[1] = game.add.tween(reelTween1);
        reelTweenGroup[2] = game.add.tween(reelTween2);
        reelTweenGroup[3] = game.add.tween(reelTween3);
        reelTweenGroup[4] = game.add.tween(reelTween4);

        multiReel = [[
            0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]];
        /*
                let reelLength = 100;
                console.log(reelLength);
                // multiReel = [[new Array(reelLength)], [new Array(reelLength)],
                // [new Array(reelLength)], [new Array(reelLength)], [new Array(reelLength)]];
                multiReel = ([[new Array(reelLength), new Array(reelLength),
                new Array(reelLength), new Array(reelLength), new Array(reelLength)]]);
                console.log(multiReel, " multiReel");
                console.log(multiReel.length, " multiReel.length");
                console.log(multiReel[0], " multiReel[0]");
                console.log(multiReel[0].length, " multiReel[0].length");
                console.log(multiReel[0][0], "multiReel[0][0]");
                let topC = 0;
                for (let gg = 0; gg < multiReel.length; gg++) {
                    for (let tt = 0; tt < reelLength; tt++) {
                        multiReel[gg][tt].push(0);
                        console.log(multiReel[gg][tt], "  multiReel[gg][tt]");
                        topC++;
                        if (gg > 3) {
                            console.log("ggggggggggggggggggggggggggggggggggggggggggggggggggggggg");
                        }
                    }
                }*/
        console.log(multiReel);
        resultArr = [[0, 0, 0], [0, 0, 0], [0, 0, 0], [0, 0, 0], [0, 0, 0]];



        multiReel = Fillreels(multiReel, NaN);





        reelTween0 = game.add.tween(spriteReel0);
        reelTween0.to({ y: -50 }, 400, Phaser.Easing.Bounce.Out, false, 0, 0, false)
            .to({ y: yMultiplicator * -reelScrollDist + 50 }, 500, Phaser.Easing.Linear.None)
            .to({ y: yMultiplicator * -reelScrollDist }, 500, Phaser.Easing.Bounce.Out);
        // reelTween0.to({y: -50}, 400, Phaser.Easing.Bounce.Out, false, 0, 0, false);
        // reelTween0.onComplete.addOnce(function() {tween2(reelTween0, spriteReel0, 0)}, this);
        reelTween0_0 = game.add.tween(spriteReel0);
        reelTween0_0.to({ y: -50 }, reelTiming0, Phaser.Easing.Bounce.Out, false, 0, 0, false);
        reelTween0_1 = game.add.tween(spriteReel0);
        reelTween0_1.to({ y: yMultiplicator * -reelScrollDist + 50 }, reelTiming1, Phaser.Easing.Linear.None, false, 0, 0, false);
        reelTween0_2 = game.add.tween(spriteReel0);
        reelTween0_2.to({ y: yMultiplicator * -reelScrollDist }, reelTiming2, Phaser.Easing.Bounce.Out, false, 0, 0, false);

        reelTween1 = game.add.tween(spriteReel1);
        reelTween1.to({ y: -50 }, 400, Phaser.Easing.Bounce.Out, false, 300, 0, false)
            .to({ y: yMultiplicator * -reelScrollDist + 50 }, 500, Phaser.Easing.Linear.None)
            .to({ y: yMultiplicator * -reelScrollDist }, 500, Phaser.Easing.Bounce.Out);

        reelTween1_0 = game.add.tween(spriteReel1);
        reelTween1_0.to({ y: -50 }, reelTiming0, Phaser.Easing.Bounce.Out, false, reelStartDelay * 1, 0, false);
        reelTween1_1 = game.add.tween(spriteReel1);
        reelTween1_1.to({ y: yMultiplicator * -reelScrollDist + 50 }, reelTiming1, Phaser.Easing.Linear.None, false, 0, 0, false);
        reelTween1_2 = game.add.tween(spriteReel1);
        reelTween1_2.to({ y: yMultiplicator * -reelScrollDist }, reelTiming2, Phaser.Easing.Bounce.Out, false, 0, 0, false);

        // reelTween1.to({y: yMultiplicator * -reelScrollDist}, 1000, Phaser.Easing.Linear.None, false, 300, 0, false);

        reelTween2 = game.add.tween(spriteReel2);
        reelTween2.to({ y: -50 }, 400, Phaser.Easing.Bounce.Out, false, 600, 0, false)
            .to({ y: yMultiplicator * -reelScrollDist + 50 }, 500, Phaser.Easing.Linear.None)
            .to({ y: yMultiplicator * -reelScrollDist }, 500, Phaser.Easing.Bounce.Out);

        reelTween2_0 = game.add.tween(spriteReel2);
        reelTween2_0.to({ y: -50 }, reelTiming0, Phaser.Easing.Bounce.Out, false, reelStartDelay * 2, 0, false);
        reelTween2_1 = game.add.tween(spriteReel2);
        reelTween2_1.to({ y: yMultiplicator * -reelScrollDist + 50 }, reelTiming1, Phaser.Easing.Linear.None, false, 0, 0, false);
        reelTween2_2 = game.add.tween(spriteReel2);
        reelTween2_2.to({ y: yMultiplicator * -reelScrollDist }, reelTiming2, Phaser.Easing.Bounce.Out, false, 0, 0, false);

        reelTween3 = game.add.tween(spriteReel3);
        reelTween3.to({ y: -50 }, 400, Phaser.Easing.Bounce.Out, false, 900, 0, false)
            .to({ y: yMultiplicator * -reelScrollDist + 50 }, 500, Phaser.Easing.Linear.None)
            .to({ y: yMultiplicator * -reelScrollDist }, 500, Phaser.Easing.Bounce.Out);

        reelTween3_0 = game.add.tween(spriteReel3);
        reelTween3_0.to({ y: -50 }, reelTiming0, Phaser.Easing.Bounce.Out, false, reelStartDelay * 3, 0, false);
        reelTween3_1 = game.add.tween(spriteReel3);
        reelTween3_1.to({ y: yMultiplicator * -reelScrollDist + 50 }, reelTiming1, Phaser.Easing.Linear.None, false, 0, 0, false);
        reelTween3_2 = game.add.tween(spriteReel3);
        reelTween3_2.to({ y: yMultiplicator * -reelScrollDist }, reelTiming2, Phaser.Easing.Bounce.Out, false, 0, 0, false);

        reelTween4 = game.add.tween(spriteReel4);
        reelTween4.to({ y: -50 }, 400, Phaser.Easing.Bounce.Out, false, 1200, 0, false)
            .to({ y: yMultiplicator * -reelScrollDist + 50 }, 500, Phaser.Easing.Linear.None)
            .to({ y: yMultiplicator * -reelScrollDist }, 500, Phaser.Easing.Bounce.Out);

        reelTween4_0 = game.add.tween(spriteReel4);
        reelTween4_0.to({ y: -50 }, reelTiming0, Phaser.Easing.Bounce.Out, false, reelStartDelay * 4, 0, false);
        reelTween4_1 = game.add.tween(spriteReel4);
        reelTween4_1.to({ y: yMultiplicator * -reelScrollDist + 50 }, reelTiming1, Phaser.Easing.Linear.None, false, 0, 0, false);
        reelTween4_2 = game.add.tween(spriteReel4);
        reelTween4_2.to({ y: yMultiplicator * -reelScrollDist }, reelTiming2, Phaser.Easing.Bounce.Out, false, 0, 0, false);

        /*
        this.reelTween0 = game.add.tween(spriteReel0);
        let tmpTween:Phaser.Tween[] = [this.reelTween0, this.reelTween1];

        tmpTween[0].to({y: yMultiplicator * -reelScrollDist}, 1000, Phaser.Easing.Linear.None, true, 0, 0, false);
        tmpTween[1].to({y: yMultiplicator * -reelScrollDist}, 1000, Phaser.Easing.Linear.None, true, 0, 0, false);




        let tweenReelDelay = 300;
        for(let index = 1; index < this.groupReel.length; index++){

            //this.reelTweenGroup[index] = game.add.tween(this.groupReel[index]);
            let tempTween = this.reelTweenGroup[index];
            console.log("A " + this.reelTweenGroup[index]);
            tempTween = game.add.tween(this.groupReel[index]);
            console.log("B " + tempTween);
            tempTween.to({y: yMultiplicator * -reelScrollDist}, 1000, Phaser.Easing.Linear.None, false, 0, tweenReelDelay, false);
            tempTween.start();
            console.log("should start here");

        }
        */

        SpriteReelsNoWinReplacement();



        mask = game.add.graphics(0, 0);
        mask.beginFill(0x000000);
        mask.drawRect(100, 140, 2000, 900);
        spriteReel0.mask = mask;
        spriteReel1.mask = mask;
        spriteReel2.mask = mask;
        spriteReel3.mask = mask;
        spriteReel4.mask = mask;



        let xPosMod = -80;
        let mod2 = 30;
        uiGroup = game.add.group();
        uisprite = game.add.sprite(91 + mod2, 23, "frame_L");
        uiGroup.add(uisprite);
        uisprite = game.add.sprite(1630 + mod2, 24, "frame_R");
        uiGroup.add(uisprite);

        uisprite = game.add.sprite(240 + xPosMod + mod2, -30, "frame_topL");
        uiGroup.add(uisprite);
        // uisprite = game.add.sprite(game.world.centerY-200 + mod, -30, "frame_topL");
        // uisprite.scale.setTo(0.75, 0.75);
        uisprite = game.add.sprite(1260 + xPosMod + mod2, 58, "frame_topR");
        uiGroup.add(uisprite);

        uisprite = game.add.sprite(444 + mod2, 20, "frame_middle_1");
        uiGroup.add(uisprite);

        uisprite = game.add.sprite(444 + 298 + mod2, 22, "frame_middle_2");
        uiGroup.add(uisprite);

        uisprite = game.add.sprite(444 + 297 + 298 + mod2, 22, "frame_middle_3");
        uiGroup.add(uisprite);

        uisprite = game.add.sprite(444 + 297 + 298 + 296 + mod2, 24, "frame_middle_4");
        uiGroup.add(uisprite);

        uisprite = game.add.sprite(game.world.centerX, 90, "logo_frame");
        uisprite.anchor.set(0.5, 0.5);

        let uiBarmod = 50;
        let uiBar = game.add.graphics(0, 0);
        uiBar.beginFill(0x151515);
        uiBar.drawRect(0, height - uiBarmod, width, uiBarmod);

        let highlightMod = 3;
        let uibarHighlight = game.add.graphics(0, 0);
        uibarHighlight.beginFill(0xEDEDED);
        uibarHighlight.drawRect(0, height - uiBarmod - highlightMod, width, highlightMod);


        this.button = game.add.button(1725, 900, "logo", actionOnClick, this, 2, 1, 0);
        this.button.scale.setTo(0.5, 0.5);
        this.button = game.add.button(1500, 900, "logo", actionOnClick, this, 2, 1, 0);
        this.button.scale.setTo(0.25);

        let style = { font: "32px Arial", fill: "#fff", boundsAlignH: "center", boundsAlignV: "middle" };
        let style2 = { font: "bold 32px Arial", fill: "#fff", boundsAlignH: "center", boundsAlignV: "middle" };
        let betAmountStatic = game.add.text(game.world.centerX + 500, height - uiBarmod / 2, "Bet Amount:", style);
        betAmountStatic.anchor.setTo(0.5);
        this.betAmount = game.add.text(game.world.centerX + 510 + betAmountStatic.width / 2, height - uiBarmod / 2, "0", style2);
        this.betAmount.anchor.setTo(0.5);
        let balanceStatic = game.add.text(game.world.centerX - 500, height - uiBarmod / 2, "Balance:", style);
        balanceStatic.anchor.setTo(0.5);
        this.balanceAmount = game.add.text(game.world.centerX - 500 + balanceStatic.width / 1.5, height - uiBarmod / 2, "0", style2);
        this.balanceAmount.anchor.setTo(0.5);

        let winStatic = game.add.text(game.world.centerX, height - uiBarmod / 2, "Won:", style);
        winStatic.anchor.setTo(0.5);
        this.winAmount = game.add.text(winStatic.position.x + winStatic.width / 1.5, height - uiBarmod / 2, "0", style2);
        this.winAmount.anchor.setTo(0.5);

        // uisprite.scale.setTo(0.75, 0.75);

        // uisprite.scale.set(0.5, 0.5);

        // console.log(function () { return leanderGMApi.getMusicAcive(); });
        let asdf = getMusic();
        console.log(asdf);

        leanderGMApi.publishEvent(leanderGMApi.publications.PLAYBET_UPDATED, 10);
        /*console.log(subToPlayRequest(function () {
            console.log("asdf");
        }));*/

        leanderGMApi.subscribeToEvent(
            leanderGMApi.publications.BROWSER_RESIZED,
            function (layoutVarsAPI) {
                // Get values from LeanderGMApi
                console.log(layoutVarsAPI);
            }
        );
        if (sumoGame.initData[0].gameMode === "FUN") {
            console.log("gamemode FUN");
            let CREDITS = 0;
            let FREE_CREDITS = 0;
            leanderGMApi.publishEvent(
                leanderGMApi.publications.BALANCE_UPDATED_GAME,
                {
                    balance: CREDITS,
                    free_balance: FREE_CREDITS,
                },
            );
        }
        // console.log(this.initData[0].params.gameMode + " gamemode");



        // test();



        // callForPlay();
    }

    /*update() {
    }*/

}
function getMusic() { return leanderGMApi.getMusicActive(); }
/*
function loadStart() {
    // hh
    console.log("i exist");
    leanderGMApi.publishEvent(leanderGMApi.publications.PREALOADING_STARTED);
    console.log("i have done my part");
}

function FileComplete() {
    // s
}

function LoadComplete() {
    // a
}
*/
function increaseBet(){

}

function initGame() {
    const subscribeInitConfirmation = leanderGMApi.subscribeToEvent(
        leanderGMApi.publications.INITIALIZED_CONFIRMED, function (xxx) {
            leanderGMApi.unsubscribeFromEvent(subscribeInitConfirmation);
            leanderGMApi.initializeGame();
            console.log(xxx);
        },
    );
    leanderGMApi.publishEvent(leanderGMApi.publications.INITIALIZED);
    leanderGMApi.publishEvent(leanderGMApi.publications.GAME_STATUS_CHANGED,
        leanderGMApi.gameStates.INITIALIZE);

}

function callForPlay() {
    console.log("");
    leanderGMApi.publishEvent(leanderGMApi.publications.GAME_STATUS_CHANGED,
        leanderGMApi.gameStates.OPEN_PLAY);
}


let sumoGame = null;

window.onload = () => {
    let requestCompletedId = leanderGMApi.subscribeToEvent(leanderGMApi.requestStatus.REQUEST_COMPLETED, function (response) {
        console.log("Successfully requested something!", response.data);
        leanderGMApi.unsubscribeFromEvent(requestCompletedId);
        leanderGMApi.unsubscribeFromEvent(requestFailedId);
        let initData = response.data;
        console.log(response.data, " hello");

        initGame();
        sumoGame = new SimpleGame(initData);
        noWinLayout = initData[0].params.noprizeReelLayout;

    });

    let requestFailedId = leanderGMApi.subscribeToEvent(leanderGMApi.requestStatus.REQUEST_FAILED, function (response) {
        console.log("Request failed.");
        leanderGMApi.unsubscribeFromEvent(requestCompletedId);
        leanderGMApi.unsubscribeFromEvent(requestFailedId);
        // tslint:disable-next-line:no-shadowed-variable
        // console.log(initData);

    });


    let initD = leanderGMApi.sendRequest({
        method: leanderGMApi.commEngine.LegaServerMethods.METHOD_INITIALIZE,
        params: {
            sessionId: leanderGMApi.getSessionId(),
            languageId:
                getQueryParamValue(queryString, "language"),
        },
    });
};
function test() {
    console.log(
        subToPlayRequest(function () {
            console.log("");
        }));
}

function structPlayPacket() {
    let playPacket = structOpenPlayPacket();
    playPacket = playPacket.concat(structPlaceBetPacket());
    playPacket = playPacket.concat(structGetBetResultPacket());
    return playPacket;
}

function subToPlayRequest(callbackFunction: Function, callbackContext?: any) {
    if (subscribePlayConfirmation) {
        leanderGMApi.unsubscribeFromEvent(subscribePlayConfirmation);
        subscribePlayConfirmation = null;
    }

    subscribePlayConfirmation = leanderGMApi.subscribeToEvent(leanderGMApi.publications.PLAY_CONFIRMED, function (response) {
        leanderGMApi.unsubscribeFromEvent(subscribePlayConfirmation);
        subscribePlayConfirmation = null;
        console.log(response);
        callbackFunction();
    });
    leanderGMApi.publishEvent(leanderGMApi.publications.PLAY_REQUEST);
}

function sendRequest(request, callBackFunction?: Function) {

    const that = this;
    const urlServer = "";
    this.serverLastRequest = request;
    console.log(request, " mushmush");

    // leanderGMApi.unsubscribeFromEvent(that.suscriptionRequestCompletedId);
    // leanderGMApi.unsubscribeFromEvent(that.suscriptionRequestFailedId);

    this.suscriptionRequestCompletedId = leanderGMApi.subscribeToEvent(leanderGMApi.requestStatus.REQUEST_COMPLETED, function (response) {
        console.log("Request Successful");
        // console.log(that.suscriptionRequestCompletedId, that.suscriptionRequestFailedId);


        leanderGMApi.unsubscribeFromEvent(that.suscriptionRequestCompletedId);
        leanderGMApi.unsubscribeFromEvent(that.suscriptionRequestFailedId);

        if (response.data[0].method === "openPlay") {
            console.log("openplay");
            // recievedOpenPlay = true;

        } else if (response.data[0].method === "settlePlay") {
            console.log("settlePlay");
        }
        console.log("logging this.saveResponseOld", saveResponseOld);
        saveResponseOld = saveResponseNew;
        saveResponseNew = response;
        console.log("logging saveResponseNew", saveResponseNew);
        /*if(response.data){
            console.log(re)
        }*/
        // that.saveResponse(response.data);
        console.log(" response? ", response);
        if (callBackFunction != null) {
            callBackFunction();
        }


    });
    // console.log(this.suscriptionRequestCompletedId);


    this.suscriptionRequestFailedId = leanderGMApi.subscribeToEvent(leanderGMApi.requestStatus.REQUEST_FAILED, function (response) {
        console.error("Request Failed.");
        leanderGMApi.unsubscribeFromEvent(that.suscriptionRequestCompletedId);
        leanderGMApi.unsubscribeFromEvent(that.suscriptionRequestFailedId);

        // that.saveResponse(response.data);

        if (callBackFunction != null) {
            callBackFunction();
        }
        // that.pubsub.publicar(LeanderMob.NotifyConst.REQUEST_FAIL, response.data);

    });
    // console.log(this.suscriptionRequestFailedId);

    leanderGMApi.sendRequest(request);
}

function structClosePlayPacket() {
    const init_packet = [{
        method: leanderGMApi.commEngine.LegaServerMethods.METHOD_CLOSE_PLAY,
        params: {
            sessionId: apiHandler.sessionID,
        },
    }];
    return init_packet;
}

/** Function that generates a structured JSON object for retrieving account balances. */
function structGetBalancePacket() {
    const init_packet = [{
        method: leanderGMApi.commEngine.LegaServerMethods.METHOD_GET_BALANCE,
        params: {
            sessionId: apiHandler.sessionID,
        },
    }];
    return init_packet;
}

/** Function that generates a structured JSON object for getting the bet result. */
function structGetBetResultPacket() {
    const init_packet = [{
        method: "getBetResult",
        params: {
            sessionId: apiHandler.sessionID,

        },
    }];
    return init_packet;
}


/** Function that creates a structured JSON object for opening a play round. */
function structOpenPlayPacket() {
    const init_packet = [{
        method: leanderGMApi.commEngine.LegaServerMethods.METHOD_OPEN_PLAY,
        params: {
            sessionId: apiHandler.sessionID,
        },
    }];
    return init_packet;
}

// TODO: Check what parameters are needed for Samurai Ken specifically.
// Save results and set symbols to results
function structPlaceBetPacket() {
    const init_packet = [{
        method: "placeBet",
        params: {
            sessionId: apiHandler.sessionID,
            type: "spin",
            bet: BET,
            params: {
                linesPlayed: 10,
            },
        },
    }];
    return init_packet;
}

/**
 * Should save the betresult.
 */
function structSaveDataPacket() {
    const init_packet = [{
        method: leanderGMApi.commEngine.LegaServerMethods.METHOD_SAVE_DATA,
        params: {
            data: {
                // betIndex: fg.betIndex,
                lines: 10,
                quickSpin: false,
                totalBet: BET,
                // bet_pos: BET_POS,

            },
            sessionId: apiHandler.sessionID,
            name: "betInfo",
        },

    },
    ];
    return init_packet;
}

function structSettlePlayPacket() {
    const init_packet = [{
        method: leanderGMApi.commEngine.LegaServerMethods.METHOD_SETTLE_PLAY,
        params: {
            sessionId: apiHandler.sessionID,
            // sessionId: fg.getSessionId()
        },
    }];
    return init_packet;
}

function structEndPlayPacket() {
    let concatPacket = structSettlePlayPacket();
    concatPacket = concatPacket.concat(structClosePlayPacket());
    return concatPacket;
}

function Click() {
    let tween = game.add.tween(logo).to({
        alpha: 0
    }, 2000, Phaser.Easing.Linear.None, false, 15);
    tween.start();

}

function actionOnClick() {
    firstActivation = false;



    console.log(sumoGame.initData[0].params.gamemode + " gamemode");
    console.log("Pushie");
    if (firstActivation) {

        console.log(sendRequest(structPlayPacket()), " logging sendRequest(structPlayPacket())");

        reelTween0_0.start();
        reelTween0_0.onComplete.addOnce(function () {
            BlurSprites(true, spriteReel0, 0); reelTween0_1.start().onComplete.addOnce(function () {
                console.log(reelTween0_1.target.position, " 888888888888888888888888888888888888888888888");
                BlurSprites(false, spriteReel0, 0); reelTween0_2.start();
            });
        }, this);

        reelTween1_0.start();
        reelTween1_0.onComplete.addOnce(function () {
            BlurSprites(true, spriteReel1, 1); reelTween1_1.start().onComplete.addOnce(function () {
                BlurSprites(false, spriteReel1, 1); reelTween1_2.start();
            });
        }, this);

        reelTween2_0.start();
        reelTween2_0.onComplete.addOnce(function () {
            BlurSprites(true, spriteReel2, 2); reelTween2_1.start().onComplete.addOnce(function () {
                BlurSprites(false, spriteReel2, 2); reelTween2_2.start();
            });
        }, this);

        reelTween3_0.start();
        reelTween3_0.onComplete.addOnce(function () {
            BlurSprites(true, spriteReel3, 3); reelTween3_1.start().onComplete.addOnce(function () {
                BlurSprites(false, spriteReel3, 3); reelTween3_2.start();
            });
        }, this);

        reelTween4_0.start();
        reelTween4_0.onComplete.addOnce(function () {
            BlurSprites(true, spriteReel4, 4); reelTween4_1.start().onComplete.addOnce(function () {
                BlurSprites(false, spriteReel4, 4); reelTween4_2.start();
            });
        }, this);

        reelTween4_2.onComplete.addOnce(() => {
            sendRequest(structEndPlayPacket());
            for (let counter = 0; counter < 5; counter++) {
                fillResults(counter);
            }
            // CheckResults();
            // ScaleWinning(0);
        }, this);
        firstActivation = false;
    } else {
        console.log("else Pushie");
        reelTiming0 = 4000;
        reelTiming1 = 750 + 4000;
        reelTiming2 = 5000;
        if (!reelTween0_0.isRunning && !reelTween0_1.isRunning
            && !reelTween0_2.isRunning && !reelTween4_0.isRunning
            && !reelTween4_1.isRunning && !reelTween4_2.isRunning) {
            sendRequest(structPlayPacket());
            ResetReels(true, NaN);

            spriteReel0.y = 0;
            spriteReel1.y = 0;
            spriteReel2.y = 0;
            spriteReel3.y = 0;
            spriteReel4.y = 0;


            // reelTween0_0.start();
            /*
            reelTween0_0.onComplete.addOnce(function () {
                BlurSprites(true, spriteReel0, 0);
                reelTween0_1.start().onComplete.addOnce(function () {
                    BlurSprites(false, spriteReel0, 0);
                    reelTween0_2.start();
                });
            }, this);*/
            AddTween(spriteReel0, 0, 0);
            /*
            reelTween1_0.start();
            reelTween1_0.onComplete.addOnce(function () {
                BlurSprites(true, spriteReel1, 1); reelTween1_1.start().onComplete.addOnce(function () {
                    BlurSprites(false, spriteReel1, 1); reelTween1_2.start();
                });
            }, this);
            */
            let qq = AddTween(spriteReel1, 0, 1);
            /*qq.onComplete.addOnce(function () {
                qq = AddTween(spriteReel1, 1, 1);
                console.log("fuck you code!");
            }, this);*/
            AddTween(spriteReel2, 0, 2);
            /*
            reelTween2_0.start();
            reelTween2_0.onComplete.addOnce(function () {
                BlurSprites(true, spriteReel2, 2); reelTween2_1.start().onComplete.addOnce(function () {
                    BlurSprites(false, spriteReel2, 2); reelTween2_2.start();
                });
            }, this);
            */
            AddTween(spriteReel3, 0, 3);
            /*
            reelTween3_0.start();
            reelTween3_0.onComplete.addOnce(function () {
                BlurSprites(true, spriteReel3, 3); reelTween3_1.start().onComplete.addOnce(function () {
                    BlurSprites(false, spriteReel3, 3); reelTween3_2.start();
                });
            }, this);
            */
            AddTween(spriteReel4, 0, 4);
            /*
            reelTween4_0.start();
            reelTween4_0.onComplete.addOnce(function () {
                BlurSprites(true, spriteReel4, 4); reelTween4_1.start().onComplete.addOnce(function () {
                    BlurSprites(false, spriteReel4, 4); reelTween4_2.start();
                });
            }, this);
            */

            // matar in resultaten in i en multidimensionell array för de 3*5 symboler som visas;
            reelTween4_2.onComplete.addOnce(() => {
                sendRequest(structEndPlayPacket());
                for (let counter = 0; counter < 5; counter++) {
                    fillResults(counter);

                }
                // CheckResults();


            }, this);
        }
    }
}


function fillResults(reelIndex: number) {

    let symbolIndex = reelScrollDist;
    
    let indexTop = symbolIndex + 3;
    let counter = 0;
    for (let index = symbolIndex; index < indexTop; index++) {
        resultArr[reelIndex][counter] = multiReel[reelIndex][index];
        counter++;
    }
    return null;
}
function fillResponse(startindex: number) {
    
    let index = 0;
    let indexShift = reelScrollDist;


    for(let i = 2; i <= 0; i--){
        multiReel[startindex][i] = saveResponseNew[2].params.value[i];
    }
    for(let symbolIndex = reelScrollDist + 2; symbolIndex >= reelScrollDist; symbolIndex--){
        for (let row = 0; row < 3; row++) {
            let xOffset = row * 300 + 230 + 125;
            let yOffset = row * yMultiplicator + 770 + 125;

            console.log(symbolIndex + " " + row);
            console.log(saveResponseNew[2].params);
            console.log(multiReel[row][symbolIndex] + " b4");

            multiReel[row][symbolIndex] = saveResponseNew[2].params[index];

            console.log(multiReel[row][symbolIndex] + " after");
            index++;
            if (index > 14) {
                //SpriteReelsCreation();
                console.log("fillresponse done");
            }
        }
    }
}




/// Den nya funktionen ska instället kolla för letje tecken utom de två sista vad som blir den längsta följden och ta den som först blir tre
function CheckResults() {
    let startedTweens = false;
    let winSymbolsIndexes = [[-1, -1, -1, -1, -1], [-1, -1, -1, -1, -1], [-1, -1, -1, -1, -1]];
    let longestStreak = 1;
    let currentStreak = -1;
    let streakValue = -1;
    let resultArrIndex = -1;
    let longestStreakValue = -1;
    winRowsWinningIndexes = [[-1, -1, -1, -1, -1], [-1, -1, -1, -1, -1], [-1, -1, -1, -1, -1], [-1, -1, -1, -1, -1], [-1, -1, -1, -1, -1],
    [-1, -1, -1, -1, -1], [-1, -1, -1, -1, -1], [-1, -1, -1, -1, -1], [-1, -1, -1, -1, -1], [-1, -1, -1, -1, -1], [-1, -1, -1, -1, -1],
    [-1, -1, -1, -1, -1], [-1, -1, -1, -1, -1], [-1, -1, -1, -1, -1], [-1, -1, -1, -1, -1], [-1, -1, -1, -1, -1], [-1, -1, -1, -1, -1],
    [-1, -1, -1, -1, -1], [-1, -1, -1, -1, -1], [-1, -1, -1, -1, -1], [-1, -1, -1, -1, -1], [-1, -1, -1, -1, -1], [-1, -1, -1, -1, -1],
    [-1, -1, -1, -1, -1], [-1, -1, -1, -1, -1]];
    for (let row = 0; row < winRows.length; row++) {
        console.log("in row For Loop");
        longestStreak = -1;
        currentStreak = 0;
        streakValue = -1;
        longestStreakValue = -1;
        resultArrIndex = -1;
        /* console.log("////////////////////////////////////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////");
        console.log(winRows[row]);
        console.log(resultArr[0][winRows[row][0]]+" "+resultArr[1][winRows[row][1]]+" "+
        resultArr[2][winRows[row][2]]+" "+resultArr[3][winRows[row][3]]+" "+resultArr[4][winRows[row][4]]);*/
        for (let iteration = 0; iteration < 3; iteration++) {
            // winSymbolsIndexes[iteration] = [-1,-1,-1,-1,-1];
            for (let i = iteration; i < 5; i++) {
                // console.log("this is i " + i);
                resultArrIndex = winRows[row][i];
                // resultArrIndex = 1;
                // console.log(resultArr[i][resultArrIndex]);
                if ((streakValue === resultArr[i][resultArrIndex]) || (resultArr[i][resultArrIndex] === 0) || streakValue === 0) {
                    currentStreak++;
                    // console.log("resultArr[rowIndex][resultArrIndex] " + resultArr[i][resultArrIndex]);

                    if (resultArr[i][resultArrIndex] > 0) {
                        streakValue = resultArr[i][resultArrIndex];
                        // console.log("current streakvalue ==== " + streakValue);
                    } else if (streakValue < 0) {
                        streakValue = 0;
                    }
                    if (streakValue === 0 && resultArr[i][resultArrIndex] !== 0) {
                        streakValue = resultArr[i][resultArrIndex];
                    }
                    // console.log("Contiuing streak of " + streakValue + " currentstreak ==== " + currentStreak);
                    if (currentStreak > longestStreak) {
                        // console.log("currentStreak = " + currentStreak + " > longeststreak = " +  longestStreak);
                        longestStreak = currentStreak;
                        longestStreakValue = streakValue;

                        // winRowsWinningIndexes[row][i] = winRows[row][i];
                        winSymbolsIndexes[iteration] = [-1, -1, -1, -1, -1];
                        // winSymbolsIndexes[iteration][i] = winRows[row][i];
                        // winSymbolsIndexes[iteration][i-1] = winRows[row][i-1];
                        // console.log(winSymbolsIndexes[iteration]);
                        if (longestStreak > 2) {
                            for (let rr = 0; rr < longestStreak; rr++) {
                                winSymbolsIndexes[iteration][i - rr] = winRows[row][i - rr];
                                console.log("rr " + rr + " longestStreak " + longestStreak + " i " + i);
                            }
                        }
                        /*
                        for(let g = longestStreak; g > (longestStreak - i); g--){
                            console.log("replaceing retroactively ");
                            winSymbolsIndexes[iteration][g] = winRows[row][g];
                            console.log(g + " - " + i + " - " + longestStreak );
                        }*/

                    }
                } else {
                    // console.log(streakValue + " !== " + resultArr[i][resultArrIndex] + " && " + resultArr[i][resultArrIndex] + " !== 0");
                    /* if(currentStreak > longestStreak){
                         longestStreak = currentStreak;
                         longestStreakValue = streakValue;
                     }*/

                    if (longestStreak < 3) {
                        let something = currentStreak;
                        // console.log("something = " + something);
                        for (let qq = i; qq > (i - something); qq--) {

                            // console.log("removing from arraypositions. qq = " + qq + " i-something = " + (i -something));
                            winSymbolsIndexes[iteration][qq - 1] = -1;
                        }
                        // console.log(winSymbolsIndexes[iteration]);
                        // winRowsWinningIndexes[row] = [0,0,0,0,0];
                    }
                    currentStreak = 1;
                    streakValue = resultArr[i][resultArrIndex];
                    // console.log("streakValue = " + streakValue);
                }
                // console.log("winSymbolsIndexes[" + iteration + "] = " + winSymbolsIndexes[iteration])
                // multiarray for different iterations here

            }
            if (longestStreak > 2) {
                // console.log("win on row " + row + " with length " + longestStreak + " with value " + longestStreakValue);
                // winRowsWinningIndexes[row][winRows[row][i]] = winRows[row][i];


            }
            currentStreak = 0;
            streakValue = -1;
        }
        // console.log(winSymbolsIndexes[0] + " " + winSymbolsIndexes[1] + " " + winSymbolsIndexes[2]);

        let winIteration = [0, 0, 0];
        let winIt = 0;
        for (winIt; winIt < 3; winIt++) {

            for (let winI = 0; winI < 5; winI++) {

                if (winSymbolsIndexes[winIt][winI] !== -1) {
                    winIteration[winIt]++;

                }
            }
        }

        let tempLenght = 0;
        if (winIteration[0] >= winIteration[1] && winIteration[0] >= winIteration[2]) {
            winRowsWinningIndexes[row] = winSymbolsIndexes[0];
            tempLenght = winIteration[0];
            // console.log("first iteration is largest");
        } else if (winIteration[1] > winIteration[0] && winIteration[1] >= winIteration[2]) {
            winRowsWinningIndexes[row] = winSymbolsIndexes[1];
            tempLenght = winIteration[1];
            // console.log("second iteration is largest");
        } else if (winIteration[2] > winIteration[0] && winIteration[2] > winIteration[1]) {
            winRowsWinningIndexes[row] = winSymbolsIndexes[2];
            tempLenght = winIteration[2];
            // console.log("third iteration is largets");
        }

        if (tempLenght > 2) {
            console.log("!");
            if (!startedTweens) {
                startedTweens = true;
                // ScaleWinning(row);
            }

            console.log("win on " + row + " with symbols " + winRowsWinningIndexes[row] +
                " with length " + tempLenght + " with type " + longestStreakValue +
                " &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&");
        }
    }
}

function ScaleWinning(row) {
    let indexShift = 0;
    let afterfirst = false;
    let tweenDelay = 500;
    let wonOnRow = false;

    console.log("in ScaleWinning");
    let tween;
    for (let index = 0; index < 5; index++) {
        console.log(winRowsWinningIndexes[row][index]);
        if (winRowsWinningIndexes[row][index] !== -1) {

            if (!wonOnRow) {
                wonOnRow = true;
                DrawWinLine(row);
            }
            indexShift = reelScrollDist;
            // console.log(groupReel[index]);
            // console.log(groupReel[index][indexShift+winRows[row][index]]);
            // console.log(groupReel[index].getChildAt(0));

            // let scaleTween = game.add.tween(groupReel[index].getChildAt(indexShift+winRows[row][index]));
            // AddTween(console.log(groupReel[index].getChildAt(index)));
            console.log("apply tween");
            tween = game.add.tween(groupReel[index].getChildAt(indexShift + winRows[row][index]).scale).to({
                x: 2, y: 2
            }, tweenDelay / 2, Phaser.Easing.Linear.None, false, tweenDelay * winOffset, 0, true);
            tween.start();

            // AddTween();
            afterfirst = true;

            // console.log("start tween");

            if (index === 4 && row < 24) {
                winOffset++;
                afterfirst = false;
                tween.onComplete.addOnce(function () { ScaleWinning(row + 1); });
            }
            if (index === 4 && row === 24) {
                winOffset = 0;
            }
        }
        if (afterfirst && winRows[row][index] !== -1) {
            if (row < 25) {
                tween.onComplete.addOnce(function () { ScaleWinning(row + 1); });
            } else {
                console.log("finished ScaleWinning");
                winOffset = 0;
            }
            if (index === 4) {
                afterfirst = false;
            }
        }
    }
}

/*
reelTween1_0 = game.add.tween(spriteReel1);
reelTween1_0.to({ y: -50 }, reelTiming0, Phaser.Easing.Bounce.Out, false, reelStartDelay * 1, 0, false);
reelTween1_1 = game.add.tween(spriteReel1);
reelTween1_1.to({ y: yMultiplicator * -reelScrollDist + 50 }, reelTiming1, Phaser.Easing.Linear.None, false, 0, 0, false);
reelTween1_2 = game.add.tween(spriteReel1);
reelTween1_2.to({ y: yMultiplicator * -reelScrollDist }, reelTiming2, Phaser.Easing.Bounce.Out, false, 0, 0, false);
*/
function forceTrue() {
    recievedOpenPlay = true;
    console.log(recievedOpenPlay, spriteReel1);
}
function forceFalse() {
    recievedOpenPlay = false;
    console.log(recievedOpenPlay, spriteReel1);
}
function forcePos(val) {
    if (val === 0) {
        spriteReel1.position.y = 0;
    }
    spriteReel1.position.y += 300 * val;
}

function AddTween(_reel, tweenId, reelId) {
    /*if (tweenId === 2) {
        _reel.position.y = -50;
    }*/
    if (_reel.position.y === NaN) {
        console.log("is NaN");
    }
    console.log(game.tweens.getAll().length, "==========================================");
    game.tweens.removeFrom(_reel);

    console.log(_reel, tweenId, reelId);
    let _tween = game.add.tween(_reel);
    let isLongTween = false;
    if (tweenId === 0) {
        console.log("start the first tween", _reel);
        _tween.to({ y: -50 }, reelTiming0, Phaser.Easing.Bounce.Out, false, reelStartDelay * reelId, 0, false);
    } else if (tweenId === 1) {
        console.log("start the second tween from position ", _reel.position.y, _reel);
        console.log("yMultiplicator * (-reelScrollDist - reelScrollDistTraveled) + 50",
            yMultiplicator * (-reelScrollDist) + 50);
        _tween.to({ y: yMultiplicator * (-reelScrollDist) + 50 },
            reelTiming1, Phaser.Easing.Linear.None, false, 0, 0, false);
    } else {
        console.log("start the third tween", _reel);
        _tween.to({ y: yMultiplicator * -reelScrollDist }, reelTiming2, Phaser.Easing.Bounce.Out, false, 0, 0, false);
    }
    _tween.start();


    /*let tween = game.add.tween(_reel).to({
        alpha: 0
    }, 2000, Phaser.Easing.Linear.None, false, 15);*/
    // tween.start();

    _tween.onComplete.addOnce(function () {
        console.log(_reel.position.y, "TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT");
        if (tweenId === 0) {
            console.log("start second tween on ", _reel);
            BlurSprites(true, _reel, reelId);
            AddTween(_reel, tweenId + 1, reelId);
        } else if (tweenId === 1) {
            if (!recievedOpenPlay) {
                console.log("not recieved responce yet, keep spinning");
                console.log(spriteReel1.position.y);
                ResetReels(true, reelId);
                BlurSprites(false, _reel, reelId);
                _reel.position.y = 0;
                /* if (reelScrollDistTraveled === 0) {
                    reelScrollDistTraveled = 10;
                }*/

                // reelScrollDistTraveled += reelScrollDist;
                AddTween(_reel, tweenId, reelId);
            } else {
                console.log("recieved response ending");
                if(!setVisibleWiningSymbols){
                    setVisibleWiningSymbols = true;
                    AddTween(_reel, tweenId, reelId);
                    fillResults(reelId);
                    BlurSprites(false, _reel, reelId);
                    
                }else{
                    
                    AddTween(_reel, tweenId + 1, reelId);
                }
            }
            /*
            BlurSprites(true, spriteReel2, 2); reelTween2_1.start().onComplete.addOnce(function () {
                BlurSprites(false, spriteReel2, 2); reelTween2_2.start();
            });*/

        } else if (tweenId === 2) {
            console.log("tween do nothing");
            setVisibleWiningSymbols = false;
            // BlurSprites(false, _reel, reelId);
            if (reelId === 4 && tweenId === 2) {
                console.log("finished all tweens on all reels");
                //change Balance and win texts here
                sumoGame.balanceAmount.set(saveResponseNew[0].params.balance);
                sumoGame.winAmount.set(saveResponseNew[0].params.winAmount);
            }
        }
    }, this);
    return _tween;
}

function DrawWinLine(row) {
    for (let index = 0; index < 5; index++) {
        // winRows[row][index];
    }
}

function BlurSprites(blur: boolean, reelGroup, reelNumber) {
    // console.log(reelGroup.length);
    // console.log(spriteReel0.getChildAt(0));
    // console.log(groupReel[0].length);
    // console.log();
    let indexShift = 0;


    for (let index2 = 2; index2 > -1; index2--) {


        if (!blur) {
            indexShift = reelScrollDist + index2;
        } else {
            indexShift = index2;
        }
        let spr;
        let xOffset2 = reelNumber * 300 + 230 + 125;
        let yOffset2 = indexShift * yMultiplicator + 770 + 125;
        groupReel[reelNumber].removeChildAt(indexShift);
        if (multiReel[reelNumber][indexShift] === 0) {
            if (blur) {
                spr = groupReel[reelNumber].addAt(reelGroup.create(xOffset2, yOffset2, "Blurred_Wild"), indexShift);
            } else {
                spr = groupReel[reelNumber].addAt(reelGroup.create(xOffset2, yOffset2, "Wild"), indexShift);
            }
        } else if (multiReel[reelNumber][indexShift] === 1) {
            if (blur) {
                spr = groupReel[reelNumber].addAt(reelGroup.create(xOffset2, yOffset2, "Blurred_Anger"), indexShift);
            } else {
                spr = groupReel[reelNumber].addAt(reelGroup.create(xOffset2, yOffset2, "Anger"), indexShift);
            }
        } else if (multiReel[reelNumber][indexShift] === 2) {
            if (blur) {
                spr = groupReel[reelNumber].addAt(reelGroup.create(xOffset2, yOffset2, "Blurred_Disgust"), indexShift);
            } else {
                spr = groupReel[reelNumber].addAt(reelGroup.create(xOffset2, yOffset2, "Disgust"), indexShift);
            }
        } else if (multiReel[reelNumber][indexShift] === 3) {
            if (blur) {
                spr = groupReel[reelNumber].addAt(reelGroup.create(xOffset2, yOffset2, "Blurred_Sad"), indexShift);
            } else {
                spr = groupReel[reelNumber].addAt(reelGroup.create(xOffset2, yOffset2, "Sad"), indexShift);
            }
        } else if (multiReel[reelNumber][indexShift] === 4) {
            if (blur) {
                spr = groupReel[reelNumber].addAt(reelGroup.create(xOffset2, yOffset2, "Blurred_Fear"), indexShift);
            } else {
                spr = groupReel[reelNumber].addAt(reelGroup.create(xOffset2, yOffset2, "Fear"), indexShift);
            }
        } else if (multiReel[reelNumber][indexShift] === 5) {
            if (blur) {
                spr = groupReel[reelNumber].addAt(reelGroup.create(xOffset2, yOffset2, "Blurred_10"), indexShift);
            } else {
                spr = groupReel[reelNumber].addAt(reelGroup.create(xOffset2, yOffset2, "10"), indexShift);
            }
        } else if (multiReel[reelNumber][indexShift] === 6) {
            if (blur) {
                spr = groupReel[reelNumber].addAt(reelGroup.create(xOffset2, yOffset2, "Blurred_A"), indexShift);
            } else {
                spr = groupReel[reelNumber].addAt(reelGroup.create(xOffset2, yOffset2, "A"), indexShift);
            }
        } else if (multiReel[reelNumber][indexShift] === 7) {
            if (blur) {
                spr = groupReel[reelNumber].addAt(reelGroup.create(xOffset2, yOffset2, "Blurred_K"), indexShift);
            } else {
                spr = groupReel[reelNumber].addAt(reelGroup.create(xOffset2, yOffset2, "K"), indexShift);
            }
        } else if (multiReel[reelNumber][indexShift] === 8) {
            if (blur) {
                spr = groupReel[reelNumber].addAt(reelGroup.create(xOffset2, yOffset2, "Blurred_Q"), indexShift);
            } else {
                spr = groupReel[reelNumber].addAt(reelGroup.create(xOffset2, yOffset2, "Q"), indexShift);
            }
        } else if (multiReel[reelNumber][indexShift] === 9) {
            if (blur) {
                spr = groupReel[reelNumber].addAt(reelGroup.create(xOffset2, yOffset2, "Blurred_J"), indexShift);
            } else {
                spr = groupReel[reelNumber].addAt(reelGroup.create(xOffset2, yOffset2, "J"), indexShift);
            }
        }
        if (spr !== undefined) {
            spr.anchor.set(0.5, 0.5);
        }
    }
}


// genererar siffrorna som symbolerna placers efter
function Fillreels(reels, reelId) {
    if(reelId != NaN){
        for (let reel = 0; reel < reels.length; reel++) {
            for (let index = 0; index < reels[reel].length; index++) {
                let symbolVal = Math.floor(Math.random() * 10);
                reels[reel][index] = symbolVal;

            }
        }
    }else {

    }
    return reels;
    
}

//  genererar nya reels och sätter de som är de första tre indexen till de som synns på den raden.
function ResetReels(reDraw: boolean, reelId: number) {
    if(reelId != NaN){
        multiReel = Fillreels(multiReel, NaN);
        for (let count = 0; count < groupReel.length; count++) {
            groupReel[count].position.y = 0;
            for (let visibleSymbols = 0; visibleSymbols < 3; visibleSymbols++) {
                multiReel[count][visibleSymbols] = resultArr[count][visibleSymbols];
            }
    
        }
        for (let i = 0; i < groupReel.length; i++) {
            groupReel[i].removeAll();
        }
    }else {
        groupReel[reelId].position.y = 0;
        for (let visibleSymbols = 0; visibleSymbols < 3; visibleSymbols++) {
            multiReel[reelId][visibleSymbols] = resultArr[reelId][visibleSymbols];
        }
    }

    if (reDraw) {
        SpriteReelsCreation(reelId);
    }
}


function ExplosionBackgroundCreator() {
    /*let explosion = game.add.image(game.world.centerX, game.world.centerY, "LightBlue_Explosion");
        explosion.anchor.set(0, 0.5);
        let explosion = game.add.image(game.world.centerX, game.world.centerY, "LightBlue_Explosion");
        explosion.anchor.set(0, 0.5);

        explosion.rotation = 0.22;*/
    let explosion;
    for (let i = 0; i < 35; i++) {
        explosion = exposionBlueGroup.create(width / 2, height / 2, "LightBlue_Explosion");
        explosion.anchor.set(0, 0.5);
        explosion.scale.set(1.2, 1);
        explosion.rotation = 0.19 * i;
    }
}
function Tween2(tween_Local: Phaser.Tween, group_Local, reelNum) {
    BlurSprites(true, group_Local, reelNum);
    tween_Local.to({ y: yMultiplicator * -reelScrollDist + 50 }, 500, Phaser.Easing.Linear.None);
    tween_Local.onComplete.addOnce(function () { Tween3(tween_Local, group_Local, reelNum); });
}

function Tween3(tween_Local: Phaser.Tween, group_Local, reelNum) {

    tween_Local.to({ y: yMultiplicator * -reelScrollDist }, 500, Phaser.Easing.Bounce.Out);
    // tween_Local.onComplete.addOnce(function() {Tween3(tween_Local, group_Local, reelNum)});
    BlurSprites(false, group_Local, reelNum);
}



// skapar och placerar sprites i grupper;
function SpriteReelsNoWinReplacement() {
    console.log(sumoGame.initData[0].gameMode, " yooo");
    let index = 0;
    for (let row = 2; row > -1; row--) {
        for (let symbolIndex = 0; symbolIndex < 5; symbolIndex++) {
            let xOffset = symbolIndex * 300 + 230 + 125;
            let yOffset = symbolIndex * yMultiplicator + 770 + 125;

            console.log(row + " " + symbolIndex);
            console.log(sumoGame.initData[0].params.noPrizeReelLayout[index]);
            console.log(multiReel[symbolIndex][row] + " b4");
            multiReel[symbolIndex][row] = sumoGame.initData[0].params.noPrizeReelLayout[index];
            console.log(multiReel[symbolIndex][row] + " after");
            index++;
            if (index > 14) {
                SpriteReelsCreation(NaN);
                console.log("SpriteReelsCreationPLacement");
            }
        }
    }
}

function SpriteReelsCreation(reelId:number) {
    
    let reelLength = multiReel.length;
    let startAtReel = 0;
    if(reelId != NaN){
        startAtReel = reelId;
        reelLength = reelId + 1;
        console.log("");
    }
    let spr;
    for (let reel = startAtReel; reel < reelLength; reel++) {
        for (let symbolIndex = 0; symbolIndex < multiReel[reel].length; symbolIndex++) {
            let xOffset = reel * 300 + 230 + 125;
            let yOffset = symbolIndex * yMultiplicator + 770 + 125;
            if (multiReel[reel][symbolIndex] === 0) {
                if (symbolIndex < 3) {
                    spr = groupReel[reel].create(xOffset, yOffset, "Wild");
                } else {
                    spr = groupReel[reel].create(xOffset, yOffset, "Blurred_Wild");
                }
            } else if (multiReel[reel][symbolIndex] === 1) {
                if (symbolIndex < 3) {
                    spr = groupReel[reel].create(xOffset, yOffset, "Anger");
                } else {
                    spr = groupReel[reel].create(xOffset, yOffset, "Blurred_Anger");
                }
            } else if (multiReel[reel][symbolIndex] === 2) {
                if (symbolIndex < 3) {
                    spr = groupReel[reel].create(xOffset, yOffset, "Disgust");
                } else {
                    spr = groupReel[reel].create(xOffset, yOffset, "Blurred_Disgust");
                }
            } else if (multiReel[reel][symbolIndex] === 3) {
                if (symbolIndex < 3) {
                    spr = groupReel[reel].create(xOffset, yOffset, "Sad");
                } else {
                    spr = groupReel[reel].create(xOffset, yOffset, "Blurred_Sad");
                }
            } else if (multiReel[reel][symbolIndex] === 4) {
                if (symbolIndex < 3) {
                    spr = groupReel[reel].create(xOffset, yOffset, "Fear");
                } else {
                    spr = groupReel[reel].create(xOffset, yOffset, "Blurred_Fear");
                }
            } else if (multiReel[reel][symbolIndex] === 5) {
                if (symbolIndex < 3) {
                    spr = groupReel[reel].create(xOffset, yOffset, "10");
                } else {
                    spr = groupReel[reel].create(xOffset, yOffset, "Blurred_10");
                }
            } else if (multiReel[reel][symbolIndex] === 6) {
                if (symbolIndex < 3) {
                    spr = groupReel[reel].create(xOffset, yOffset, "A");
                } else {
                    spr = groupReel[reel].create(xOffset, yOffset, "Blurred_A");
                }
            } else if (multiReel[reel][symbolIndex] === 7) {
                if (symbolIndex < 3) {
                    spr = groupReel[reel].create(xOffset, yOffset, "K");
                } else {
                    spr = groupReel[reel].create(xOffset, yOffset, "Blurred_K");
                }
            } else if (multiReel[reel][symbolIndex] === 8) {
                if (symbolIndex < 3) {
                    spr = groupReel[reel].create(xOffset, yOffset, "Q");
                } else {
                    spr = groupReel[reel].create(xOffset, yOffset, "Blurred_Q");
                }
            } else if (multiReel[reel][symbolIndex] === 9) {
                if (symbolIndex < 3) {
                    spr = groupReel[reel].create(xOffset, yOffset, "J");
                } else {
                    spr = groupReel[reel].create(xOffset, yOffset, "Blurred_J");
                }
            }
            if (spr !== undefined) {
                spr.anchor.set(0.5, 0.5);
            }
        }
    }
}
