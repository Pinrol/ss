export interface FreePlaysData { }

export interface CasinoData { }

export interface ResponseHistory {
    sessionId: string;
    playId: number;
    method: string;
}

export interface Value {
    1: number;
    2: number;
    3: number;
    4: number;
    5: number;
}


export interface PayTable {
    1: 1;
    2: 2;
    3: 3;
    4: 4;
    5: 5;
    6: 6;
    7: 7;
    8: 8;
}

export interface Param {
    currencyDecimalPlaces: number;
    responseHistory: ResponseHistory[];
    autoPlayDefault: number;
    lineLayout: number[][];
    betPerLineDefault: number;
    betPerLine: number[];
    noPrizeReelLayout: number[];
    currencySuffix: string;
    autoPlayList: number[];
    currencyPrefix: string;
    payTable: PayTable;
}

export interface Data { }

export interface IinitData {
    jackpotsInfo: any[];
    gameRTP: number;
    currencyId: string;
    sessionExpirationTimeoutSeconds: number;
    gameMode: string;
    freePlaysData: FreePlaysData;
    casinoData: CasinoData;
    method: string;
    sessionId: string;
    gameRTPJP?: any;
    params: Param;
    maxWinnings: number;
    gameRTPUP?: any;
    balance: number;
    data: Data;
    freeBalance: number;
}

